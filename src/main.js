// stylesheets for bootstrap vue
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';

import Vue from 'vue';
import Vuex from 'vuex';
import Router from 'vue-router';
import App from './App.vue';
import axios from 'axios';
import VueAxios from 'vue-axios';
import { sync } from 'vuex-router-sync';
import VuexStore from './vuex/store';
import { routes } from './router-config';
import BootstrapVue from 'bootstrap-vue';
import VueSweetAlert from 'vue-sweetalert';
import VueSocketio from 'vue-socket.io';
// set up config file as global frontend config
import vueConfig from 'vue-config';
import config from './config/index';

Vue.use(vueConfig, config);

// set up axios as http requests tool (this.$http)
Vue.use(VueAxios, axios);
Vue.axios.defaults.baseURL = config.backendUrl;

Vue.use(VueSocketio, config.backendUrl);

// events lib for handling global events
import VueEvents from 'vue-events'
Vue.use(VueEvents);

// store and router
Vue.use(Vuex);
Vue.use(Router);
const store = new Vuex.Store(VuexStore);
const router = new Router({
    routes,
    mode: 'history',
    scrollBehavior (to, from, savedPosition) {
        if (from.name === to.name) {
            return savedPosition;
        }
        if (!savedPosition || to !== from) {
            return { x: 0, y: 0 }
        }
        return savedPosition;
    },
});
sync(store, router);
Vue.router = router;

// bootstrap and swal for modal windows
Vue.use(BootstrapVue);
Vue.use(VueSweetAlert);

// auth with jwt module
Vue.use(require('@websanova/vue-auth'), {
    auth: require('./auth/authDriver.js'),
    http: require('@websanova/vue-auth/drivers/http/axios.1.x.js'),
    router: require('@websanova/vue-auth/drivers/router/vue-router.2.x.js'),
    rolesVar: 'role',
    authRedirect: {path: '/login'},
    forbiddenRedirect: {path: '/403'},
    notFoundRedirect: {path: '/404'},
    registerData: {url: 'user/register', method: 'POST', redirect: '/', fetchUser: false},
    loginData: {url: 'user/login', method: 'POST', redirect: '/', fetchUser: true},
    logoutData: {url: 'user/logout', method: 'POST', redirect: '/', makeRequest: false},
    fetchData: {url: 'user', method: 'GET', enabled: false},
    refreshData: {url: 'user', method: 'GET', enabled: false, interval: 30},
    parseUserData:  data => data,
});

const app = new Vue({
    router,
    store,
    el: '#app',
    render: h => h(App)
});
