import mimeTypes from './mimetypes';

export default {
    backendUrl: env.BACKEND_URI,
    staticUrl: env.STATIC_URI,
    mimeTypes
}