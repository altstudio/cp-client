export default (watchedFields = [], errorsField = 'errors') => {
    const watch = {};
    watchedFields.forEach(({ key, errorKey }) => {
        watch[key] = function () {
            delete this[errorsField][errorKey];
        }
    });

    return {
        data() {
            return {
                [errorsField]: {}
            }
        },
        methods: {
            isValid(key) {
                return this[errorsField].hasOwnProperty(key) ? 'invalid' : null;
            },
            errorMessage(key) {
                return this[errorsField].hasOwnProperty(key) ? this[errorsField][key].msg : '';
            }
        },
        watch
    }
}