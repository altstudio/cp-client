import Login from './pages/Login'
import Register from './pages/Register'
import Home from './pages/Home'
import Account from './pages/Account'

import ProblemCreate from './pages/Problem/Create';
import ProblemView from './pages/Problem/View';

export const routes = [
    {
        path: '/',
        name: 'home',
        component: Home
    },
    // login and register
    {
        path: '/login',
        name: 'login',
        meta: {
            auth: false
        },
        component: Login
    },
    {
        path: '/register',
        name: 'register',
        meta: {
            auth: false
        },
        component: Register
    },
    // problems
    {
        path: '/problems/create',
        name: 'problem_create',
        meta: {
            auth: true
        },
        component: ProblemCreate
    },
    {
        path: '/account',
        name: 'account',
        meta: {
            auth: true
        },
        component: Account
    },
    {
        path: '/problems/edit/:id',
        name: 'problem_edit',
        meta: {
            auth: true
        },
        component: ProblemCreate
    },
    {
        path: '/problems/:id',
        name: 'problem_view',
        props: true,
        component: ProblemView
    }
];