const path = require('path');
const webpack = require('webpack');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
require("babel-polyfill");

const env = require('dotenv').config({ path: path.join(__dirname, '.env') }).parsed;

module.exports = {
    entry: ['babel-polyfill', './src/main.js'],
    output: {
        path: path.resolve(__dirname, './public'),
        filename: 'scripts/main.js',
        publicPath: '/'
    },
    // output: {
    //     path: path.resolve(__dirname, './public'),
    //     publicPath: '/',
    //     filename: 'javascripts/build.js'
    // },
    module: {
        rules: [
            {
                test: /\.css$/,
                use: [
                    'vue-style-loader',
                    'css-loader'
                ],
            },      {
                test: /\.vue$/,
                loader: 'vue-loader',
                options: {
                    loaders: {
                    }
                    // other vue-loader options go here
                }
            },
            {
                test: /\.js$/,
                loader: 'babel-loader',
                exclude: /node_modules/
            },
            {
                test: /\.scss$/,
                use: [
                    'vue-style-loader',
                    'css-loader',
                    'sass-loader'
                ]
            },
            {
                test: /\.(png|jpg|gif|svg|ttf|otf|eot|woff2?)(\?.*)?$/,
                loader: 'file-loader',
                options: {
                    name: '[name].[ext]?[hash]',
                    outputPath: 'assets/'
                }
            }
        ]
    },
    resolve: {
        alias: {
            'vue$': 'vue/dist/vue.esm.js',
            '@': path.join(__dirname, './src')
        },
        extensions: ['*', '.js', '.vue', '.json']
    },
    devServer: {
        contentBase: path.join(__dirname, 'assets'),
        historyApiFallback: true,
        noInfo: false,
        overlay: true
    },
    performance: {
        hints: false
    },
    devtool: '#eval-source-map',
    plugins: [
        new webpack.DefinePlugin({
            'env': JSON.stringify(env)
        }),
    ]
};

// if (process.env.NODE_ENV === 'production') {
//     module.exports.devtool = '#source-map'
//     // http://vue-loader.vuejs.org/en/workflow/production.html
//     module.exports.plugins = (module.exports.plugins || []).concat([
//         new webpack.DefinePlugin({
//             'process.env': {
//                 NODE_ENV: '"production"'
//             }
//         }),
//         new UglifyJsPlugin(),
//         new webpack.LoaderOptionsPlugin({
//             minimize: true
//         })
//     ])
// }
// module.exports.plugins = (module.exports.plugins || []).concat([
    // new CopyWebpackPlugin([
    //     { from: path.join(__dirname, './assets'), to: path.join(__dirname, '../public/assets') }
    // ]),
    // new webpack.DefinePlugin({
    //     'env': JSON.stringify(env)
    // }),
// ])