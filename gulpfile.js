const gulp = require('gulp');
const del = require('del');
const compiler = require('webpack');
const webpack = require('webpack-stream');
const browserSync = require('browser-sync').create();
const notifier = require('node-notifier');

const webpackConfig = require('./webpack.config');

gulp.task('clean', () => del('./public'));
gulp.task('copy:assets', () =>
    gulp.src('assets/**/*.*', { since: gulp.lastRun('copy:assets') })
        .pipe(gulp.dest('public'))
);

const webpackTask = (watch = false) => cb => {
    const config = { ...webpackConfig, watch };
    return gulp
        .src('./src/main.js')
        .pipe(webpack({ ...webpackConfig, watch: true }, compiler))
        .on('error', function (error) {
            notifier.notify({
                title: `Error: webpack`,
                message: error.message
            });
            this.emit('end');
        })
        .pipe(gulp.dest(file => {
            if (file.extname === '.js') {
                return 'public/scripts';
            } else if (file.extname === '.css') {
                return 'public/styles';
            } else {
                return 'public'
            }
        }))
}

gulp.task('webpack', webpackTask());
gulp.task('webpack:watch', webpackTask(true));

gulp.task('watch', () => {
    gulp.watch('assets/**/*.*', { ignoreInitial: false }, gulp.series('copy:assets'));
    gulp.watch('src/**/*.*', { ignoreInitial: false }, gulp.series('webpack:watch'));
});
gulp.task('serve', () => {
    browserSync.init({
        server: {
            baseDir: './public',
        },
        port: 3001,
        single: true
    });
});

gulp.task('build', gulp.series('clean', gulp.parallel('copy:assets', 'webpack')));
gulp.task('dev', gulp.series('clean', gulp.parallel('watch', 'serve')));

gulp.task('default', gulp.series('dev'));